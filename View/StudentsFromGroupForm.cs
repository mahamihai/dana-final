﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services.Services;

namespace View
{
    public partial class StudentsFromGroupForm : Form
    {
        private GroupServices _groupServices;
        public StudentsFromGroupForm()
        {
            InitializeComponent();
            this._groupServices = new GroupServices();

            var groupNumbers = this._groupServices.getGroupNumbers();//pune numerele grupelor
            this.comboBox1.DataSource = groupNumbers;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)//cand selectezi o grupa
        {
            int groupNr = Int32.Parse(this.comboBox1.SelectedValue.ToString());
            var students = this._groupServices.getGroupStudents(groupNr);
            this.dataGridView1.DataSource = students;
        }
    }
     
       
     
    }

