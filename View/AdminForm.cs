﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services.Models;
using Services.Services;

namespace View
{
    public partial class AdminForm : Form
    {
        private AdminServices _aServices;
        private int selectedStudentId;
        private int selectedGroupId;
        private int selectedTeacherId;
        private int selectedStudyingTopic;
        public AdminForm(AdminServices aServices)
        {
            InitializeComponent();
            this._aServices = aServices;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var students = this._aServices.getStudents();
            this.dataGridView1.DataSource = students;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedStudentId = id;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this._aServices.deleteStudent(this.selectedStudentId);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string Name = this.textBox1.Text;
                string ParentMail = this.textBox2.Text;
                string password = this.textBox3.Text;
                string username = this.textBox4.Text;
                int groupNr = Int32.Parse(this.textBox5.Text);
                StudentModel newS = new StudentModel
                {
                    Name = Name,
                    ParentMail = ParentMail,
                    Password = password,
                    Username = username,
                    GroupId = groupNr
                };
                var result = this._aServices.addStudent(newS);
            }
            catch
            {
                MessageBox.Show("Not a valid input");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                string Name = this.textBox1.Text;
                string ParentMail = this.textBox2.Text;
                string password = this.textBox3.Text;
                string username = this.textBox4.Text;
                int groupNr = Int32.Parse(this.textBox5.Text);
                StudentModel newS = new StudentModel
                {
                    id=this.selectedStudentId,
                    Name = Name,
                    ParentMail = ParentMail,
                    Password = password,
                    Username = username,
                    GroupId = groupNr
                    
                };
                var result = this._aServices.updateStudent(newS,this.selectedStudentId);
            }
            catch
            {
                MessageBox.Show("Not a valid input");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var groups = this._aServices.getGroups();
            this.dataGridView2.DataSource = groups;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var teachers = this._aServices.getTeachers();
            this.dataGridView3.DataSource = teachers;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            var studiedTopics = this._aServices.getStudiedTopics(this.selectedGroupId);
            this.dataGridView4.DataSource = studiedTopics;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            TeachModel newTeach = new TeachModel
            {
                GroupId = this.selectedGroupId,
                id = this.selectedStudyingTopic,
                TeacherId = this.selectedTeacherId
            };
            this._aServices.assignTeacher(newTeach, this.selectedStudyingTopic);
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)//get selected's grpup id
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView2.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedGroupId = id;

        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)//ial id-ul profesorului selectat
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView3.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedTeacherId = id;

        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView4.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedStudyingTopic = id;

        }
    }
}
