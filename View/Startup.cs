﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services.Login;
using Services.Models;
using Services.Services;

namespace View
{
    public partial class Startup : Form
    {
        public Startup()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginService sLogin=new StudentLogin();
            string username = this.textBox1.Text;
            string password = this.textBox2.Text;
            var account = sLogin.login(username, password);
            if (account != null)
            {
                if (account.getType().Equals("Student"))
                {
                    StudentService studService=new StudentService((StudentModel)account);
                    StudentForm sForm=new StudentForm(studService);
                    sForm.Show();
                }
               

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoginService sLogin = new UserLogin();
            string username = this.textBox1.Text;
            string password = this.textBox2.Text;
            var account = sLogin.login(username, password);
            if (account != null)
            {
                if (account.getType().Equals("Teacher"))
                {
                    TeacherService tService=new TeacherService((UserModel)account);
                    TeacherForm tForm=new TeacherForm(tService);
                    tForm.Show();
                }
                else if (account.getType().Equals("Admin"))
                {
                    AdminServices aServices=new AdminServices((UserModel)account);
                    AdminForm aForm=new AdminForm(aServices);
                    aForm.Show();
                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StudentsFromGroupForm newStudentsForm=new StudentsFromGroupForm();
            newStudentsForm.Show();
        }
    }
}
