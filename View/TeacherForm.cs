﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services.Services;

namespace View
{
    public partial class TeacherForm : Form
    {
        private TeacherService _tService;
        public TeacherForm(TeacherService tService)
        {
            InitializeComponent();
            this._tService = tService;
            this.comboBox1.DataSource = this._tService.getTopics();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string topic = this.comboBox1.SelectedValue.ToString();
            var attendingStuds = this._tService.getAttendingStudents(topic);
            this.dataGridView1.DataSource = attendingStuds;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(selectedStudentId<1)
            {
                MessageBox.Show("Please select a student");
                return;
            }
            string topic = this.comboBox1.SelectedValue.ToString();
            var response = this._tService.markAbsent(this.selectedStudentId, topic);
            MessageBox.Show(response);


        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int grade = Int32.Parse(this.textBox1.Text);
                var t = this._tService.gradeStudent(this.selectedStudentId, this.comboBox1.SelectedValue.ToString(), grade);
            }
            catch
            {

            }
        }

        private int selectedStudentId;
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedStudentId = id;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string topic = this.comboBox1.SelectedValue.ToString();
            var data = this._tService.getAbsencesForTopic(topic);
            this.dataGridView2.DataSource = data;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string message = this.textBox2.Text;

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to send an email containing the message:\n"+message, "Are you sure you want to send the mail", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this._tService.sendMail(message, this.selectedStudentId);
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DateTime start = this.dateTimePicker1.Value;
            DateTime stop = this.dateTimePicker2.Value;
            string courseName = this.comboBox1.SelectedItem.ToString();
            var absences = this._tService.getAbsencesOverPeriod(start, stop, courseName);
            this.dataGridView2.DataSource = absences;
        }
    }
}
