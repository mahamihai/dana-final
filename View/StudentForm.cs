﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services.Services;

namespace View
{
    public partial class StudentForm : Form
    {
        private StudentService _sService;
        public StudentForm(StudentService _sService)
        {
            InitializeComponent();
            this._sService = _sService;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var grades = this._sService.getGrades();
            this.dataGridView1.DataSource = grades;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var absences = this._sService.getAbsence();
            this.dataGridView2.DataSource = absences;
        }
    }
}
