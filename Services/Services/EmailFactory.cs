﻿using System;
using System.Net.Mail;

namespace Services.Services
{
    public class EmailFactory
    {

        public SmtpClient getConnection()
        {
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("your.beloved.teacher@gmail.com", "teacher123!");
            SmtpServer.EnableSsl = true;
            return SmtpServer;
        }
        public void sendEmail(string email, string token)
        {
            var server = this.getConnection();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("your.beloved.teacher@gmail.com");
            mail.To.Add(email);
            mail.Subject = "Important message";
            mail.Body =  token;
            server.Send(mail);
            Console.WriteLine("Sent mail");

        }
    }
}
