﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Dal;
using Models;
using Services.Models;

namespace Services.Services
{
   public class GroupServices
   {
       private GroupRepo _gRepo;
       private IMapper mapper;
       private StudentRepo _sRepo;
        public GroupServices()
        {
            this._gRepo=new GroupRepo();
            this._sRepo=new StudentRepo();
            var config = new MapperConfiguration(cfg =>//mapeaza obiectele
            {
                cfg.CreateMap<StudentModel, Student>();
                cfg.CreateMap<Student, StudentView>();
            });
            mapper=config.CreateMapper();


        }
        public List<int> getGroupNumbers()
        {
            var groups = this._gRepo.getAll();
            var grupsNumbers = groups.Select(x => { return x.id; }).ToList();
            return grupsNumbers;
        }

       public List<StudentView> getGroupStudents(object groupId)
       {
           var students = this._sRepo.getAll();
           var filtered = students.Where(x => x.GroupId.Equals(groupId)).ToList();
           List<StudentView> converted = filtered.Select(x => mapper.Map<StudentView>(x)).ToList();
           return converted;

       }
    }
}
