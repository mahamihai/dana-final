﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Dal;
using Models;
using Services.Models;

namespace Services.Services
{
   public  class StudentService
   {
       private StudentModel _student;
       private MapperConfiguration mapper;
       private GradeRepo _gRepo;
       private CourseRepo _cRepo;
       private AbsenceRepo _aRepo;
        public StudentService(StudentModel _student)
        {
       
            mapper=new MapperConfiguration(cfg =>//mapeaza obiectele
            {
                cfg.CreateMap<StudentModel, Student>();
                cfg.CreateMap<Student, StudentModel>();
            });
            this._student = _student;
            this._gRepo=new GradeRepo();
            this._cRepo=new CourseRepo();
            this._aRepo=new AbsenceRepo();
            
        }

       public List<GradeView> getGrades()//returneaza lista de nota pentru view
       {
           var allGrades = this._gRepo.getAll().Where(x => x.studentId.Equals(this._student.id));//get all grades
           var allCourses = this._cRepo.getAll();//get all courses
           var grades = allGrades.Select(x =>//match every grade with the course name for better displaying
               {
                   return new GradeView
                   {
                       grade1 = x.grade1,
                       Name = allCourses.Find(y => y.id.Equals(x.courseId)).Name
                   };
               }

           ).ToList();
           return grades;

       }
       public List<AbsenceVIew> getAbsence()//returneaza lista de nota pentru view
       {
           var allAbsences = this._aRepo.getAll().Where(x => x.studentId.Equals(this._student.id));//get all absences
           var allCourses = this._cRepo.getAll();//get all courses
           var absences = allAbsences.Select(x =>//math absence with course name
               {
                   return new AbsenceVIew
                   {
                       CourseName = allCourses.Find(y => y.id.Equals(x.courseId)).Name,
                       Date = x.date

                   };
               }

           ).ToList();
           return absences;

       }


    }
}
