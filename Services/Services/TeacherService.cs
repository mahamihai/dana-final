﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Dal;
using Models;
using Services.Filters;
using Services.Models;

namespace Services.Services
{
  public   class TeacherService
    {
        private UserModel _user;
        private IMapper mapper;
        private GradeRepo _gRepo;
        private CourseRepo _cRepo;
        private AbsenceRepo _aRepo;
        private TeachesRepo _tRepo;
        private StudentRepo _sRepo;
     
        public TeacherService(UserModel _user)
        {

            var config = new MapperConfiguration(cfg =>//mapeaza obiectele
            {
                cfg.CreateMap<StudentModel, Student>();
                cfg.CreateMap<Student, StudentModel>();
                cfg.CreateMap<Absence, AbsenceModel>();
                cfg.CreateMap<AbsenceModel, Absence>();
            });
            this.mapper = config.CreateMapper();
            this._user = _user;
            this._gRepo = new GradeRepo();
            this._cRepo = new CourseRepo();
            this._aRepo = new AbsenceRepo();
            this._tRepo=new TeachesRepo();
            this._sRepo = new StudentRepo();

        }

        
        public string[] getTopics()//ia materiile la care preda
        {
            var allTopics = this._tRepo.getAll();
            var topics = allTopics.Where(x => x.TeacherId.Equals(this._user.id)).ToList();
            var courses = this._cRepo.getAll();
            var topicsList = topics.Select(x => { return courses.Find(y => y.id.Equals(x.CourseId)).Name; }).ToArray();
            return topicsList;
        }
        public List<StudentModel> getAttendingStudents(string topic)//ia numele cursului si spunemi cine il studiaza
        {
            var students = this._sRepo.getAll();
            var teaching = this._tRepo.getAll();
            var groups = this._gRepo.getAll();
            var courses = this._cRepo.getAll();
            var attendingStudents = students.Where(x =>
              {

                  int groupId = x.GroupId;
                  var courseId = courses.First(m => m.Name.Equals(topic)).id;
                  var beingTaught = teaching.Find(z => z.GroupId.Equals(groupId)&&z.TeacherId.Equals(this._user.id)
                  
                  &&z.CourseId.Equals(courseId));
                  return beingTaught != null;
                 

                 

              }

            ).Select(x=>mapper.Map<StudentModel>(x)).ToList();
            return attendingStudents;
        }
        public List<AbsenceModel> getAbsencesForTopic(string topic)//ii dau numele unui curs si sa imi arate absentele la cursul respectiv
        {
            var course = this._cRepo.getAll().Find(x => x.Name.Equals(topic));//

            var teachings = this._tRepo.getAll().Where(x => x.TeacherId.Equals(this._user.id)//vede care sunt absentii la un curs anume pe care il preda proful aceasta
            && x.CourseId.Equals(course.id));
            var teachingStudents = this._sRepo.getAll().Where(x =>

              {
                  var exists = teachings.First(y => y.GroupId.Equals(x.GroupId));
                  return exists != null;
              });
            //ai numele cursului si idul profului si mergi prin toate grupele si vezi daca el le preda materia respectiva si le afisezi absentele daca is elevii lui
            var absences = this._aRepo.getAll().Where(x =>
            {
                var exists = teachingStudents.Where(y => y.id.Equals(x.studentId));
                return exists != null;
            });

            
            
            
            var converted = absences.Select(x => mapper.Map<AbsenceModel>(x)).ToList();
            return converted;
            
        }
        public void sendMail(string message,int studentID)
        {
            var student = this._sRepo.getAll().Find(x => x.id.Equals(studentID));
            EmailFactory newFact = new EmailFactory();
            newFact.sendEmail(student.ParentMail, message);
        }
        public string gradeStudent(int studentId,string courseName,int grade)//da o nota unui student deal profului la un curs predat de el
        {
            try
            {
                Cours course = this._cRepo.getAll().Find(x => x.Name.Equals(courseName));
                Grade newGrade = new Grade
                {
                    grade1 = grade,
                    studentId = studentId,
                    courseId = course.id


                };
                this._gRepo.Add(newGrade);
                return "Succes";
            }
            catch
            {
                return "Error";
            }
        }
        public string markAbsent(int studId,string topic)//pune absenta la un curs predat de proful curent
        {
            try
            {
                var course = this._cRepo.getAll().First(x => x.Name.Equals(topic));
                AbsenceModel newAbsence = new AbsenceModel
                {
                    courseId = course.id,
                    studentId = studId,
                    date = DateTime.Now,

                };
                var converted = mapper.Map<Absence>(newAbsence);
                this._aRepo.Add(converted);
                return "Succes";
            }
            catch
            {
                return "Error";
            }
        }

        public List<AbsenceModel> getAbsencesOverPeriod(DateTime start, DateTime stop, string courseName)//ia absentele la un curs predat intre doyua  perioade de timp
        {
            var absences = this._aRepo.getAll();
            var course = this._cRepo.getAll().First(x => x.Name.Equals(courseName));//ia idul cursului
            List<IFilter<Absence>> filters=new List<IFilter<Absence>>();
            filters.Add(new AfterFilter(start));//creeaza filtrele
            filters.Add(new BeforeFIlter(stop));
            filters.Add(new CourseFIlter(course.id));
            filters.ForEach(x =>//aplcia fiecare filtru
                {
                    absences = x.filter(absences);
                }
                
                
                );
            var converted = absences.Select(x => mapper.Map<AbsenceModel>(x)).ToList();
            return converted;
        }
    }
}
