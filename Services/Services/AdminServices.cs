﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Dal;
using Models;
using Services.Models;

namespace Services.Services
{
   public  class AdminServices
   {
       private UserModel _user;
   
     
       private IMapper mapper;
       private GradeRepo _gRepo;
       private CourseRepo _cRepo;
       private AbsenceRepo _aRepo;
       private TeachesRepo _tRepo;
       private StudentRepo _sRepo;
       private GroupRepo _groupRepo;
       private UserRepo _userRepo;
       public AdminServices(UserModel _user)
       {

           var config = new MapperConfiguration(cfg =>//mapeaza obiectele
           {
               cfg.CreateMap<StudentModel, Student>();
               cfg.CreateMap<Student, StudentModel>();
               cfg.CreateMap<Absence, AbsenceModel>();
               cfg.CreateMap<AbsenceModel, Absence>();
               cfg.CreateMap<Group, GroupModel>();
               cfg.CreateMap<GroupModel, Group>();
               cfg.CreateMap<UserModel, User>();
               cfg.CreateMap<User, UserModel>();
               cfg.CreateMap<Teach, TeachModel>();
               cfg.CreateMap<TeachModel, Teach>();


           });
           this.mapper = config.CreateMapper();
           this._user = _user;
           this._gRepo = new GradeRepo();
           this._cRepo = new CourseRepo();
           this._aRepo = new AbsenceRepo();
           this._tRepo = new TeachesRepo();
           this._sRepo = new StudentRepo();
           this._groupRepo=new GroupRepo();
           this._userRepo = new UserRepo();



       }

        public List<StudentModel> getStudents()
        {
            var students = this._sRepo.getAll();
            return students.Select(x=>mapper.Map<StudentModel>(x)).ToList();
        }

       public string deleteStudent(object id)
       {
           try
           {
               this._sRepo.DeleteById(id);
               return "Success";
           }
           catch
           {
               return "Failure";
           }
       }

       public string addStudent(StudentModel student)
       {

           try
           {
                var converted = this.mapper.Map<Student>(student);
           this._sRepo.Add(converted);
               return "Success";
           }
           catch
           {
               return "Failure";
           }
        }
       public string updateStudent(StudentModel student,object id)
       {
            try { 

           var converted = this.mapper.Map<Student>(student);
           this._sRepo.UpdateById(converted,id);
           return "Success";
       }
       catch
       {
           return "Failure";
       }
       }
       public List<GroupModel> getGroups()//get all groups
       {
           try
           {

               var groups = this._groupRepo.getAll();
               var converted = groups.Select(x => this.mapper.Map<GroupModel>(x)).ToList();
               return converted;
           }
           catch(Exception e)
           {
               var t = e.ToString();
               return null;
           }
       }

       public List<UserModel> getTeachers()//get all teachers
       {
           var teachers = this._userRepo.getAll().Where(x => x.Type.Equals("Teacher"));
           var converted = teachers.Select(x => this.mapper.Map<UserModel>(x)).ToList();
           return converted;
       }

       public List<PrettyStudiedTopic> getStudiedTopics(int groupId)//get studied topics for a group
       {
           var courses = this._cRepo.getAll();
           var studiedTopics = this._tRepo.getAll().Where(x=>x.GroupId.Equals(groupId)).ToList();
           var teachers = this.getTeachers();//inlocuieste teacher id cu numele profului si course id cu numele cursului ca sa intelegi ceva din tabel
           var prettyFormat = studiedTopics.Select(x =>
               {
                   UserModel teacher = teachers.Find(y => y.id.Equals(x.TeacherId));
                   Cours course = courses.Find(u => u.id.Equals(x.CourseId));
                  
                   return new PrettyStudiedTopic
                   {
                       id = x.id,
                       TeacherName = teacher.Name,
                       CouseName = course.Name
                   };
               }

           ).ToList();
           return prettyFormat;
       }

       public string assignTeacher(TeachModel teach,object teachingId) //atribuie un prof la o grupa la un anumit curs
       {
           try
           {
               var oldTeaching = this._tRepo.getAll().First(x => x.id.Equals(teach.id));
               oldTeaching.TeacherId = teach.TeacherId;
               var converted = this.mapper.Map<Teach>(oldTeaching);
                this._tRepo.UpdateById(converted, teachingId);
               return "Success";

           }
           catch (Exception e)
           {
               var t = e.ToString();
               return "Fail";
           }
        }
   }
}
