﻿using AutoMapper;
using DataAccess.Dal;
using Models;
using Services.Models;

namespace Services.Login
{
   public class UserLogin:LoginService
    {
        public override Account login(string username, string password)//override to be decorator DP
        {
            var user = this._uRepo.getAll().Find(x => x.username.Equals(username) && x.password.Equals(password));
            return mapper.Map<UserModel>(user);
        }

        private IMapper mapper;

        private UserRepo _uRepo;
        public UserLogin()
        {
            this._uRepo = new UserRepo();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<UserModel, User>();
            });
            mapper = new Mapper(config);
        }
   
    }
}
