﻿using AutoMapper;
using DataAccess.Dal;
using Models;
using Services.Models;

namespace Services.Login
{
    public class StudentLogin:LoginService
    {
        private StudentRepo _sRepo;
        private IMapper mapper;
        public StudentLogin()
        {
            this._sRepo = new StudentRepo();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<StudentModel, Student>();
                cfg.CreateMap<Student, StudentModel>();
            });
          mapper=new Mapper(config);
        }
        public override Account login(string username, string password)//override to be decorator DP
        {
            var student = this._sRepo.getAll().Find(x => x.Password.Equals(password) && x.Username.Equals(username));
            var useless = this._sRepo.getAll();
            return mapper.Map<StudentModel>(student);


        }
    }
}
