﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
    public class CourseModel
    {

        public int id { get; set; }
        public string Name { get; set; }
        public Nullable<int> TeacherID { get; set; }

      
    }
}
