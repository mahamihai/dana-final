﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
   public  class PrettyStudiedTopic
    {
        //clasa asta e ca sa afisezi ce prof preda ce curs la o anumita grupa
        public int id { get; set; }
        public string TeacherName { get; set; }
        public string CouseName { get; set; }
    }
}
