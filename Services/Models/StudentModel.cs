﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
    public class StudentModel:Account
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string ParentMail { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public int GroupId { get; set; }
        public string getType()
        {
            return "Student";
        }
    }
}
