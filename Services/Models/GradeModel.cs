﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
    public class GradeModel
    {
        public int id { get; set; }
        public int courseId { get; set; }
        public int studentId { get; set; }
        public int grade1 { get; set; }
    }
}
