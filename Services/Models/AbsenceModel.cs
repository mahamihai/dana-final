﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
  public  class AbsenceModel
    {
        public int id { get; set; }
        public Nullable<int> studentId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public Nullable<int> courseId { get; set; }
    }
}
