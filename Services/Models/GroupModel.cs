﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
    public  class GroupModel
    {
        public int id { get; set; }
        public string Specialization { get; set; }
    }
}
