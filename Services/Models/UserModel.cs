﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
   public  class UserModel:Account
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public string getType()
        {
            return this.Type;
        }
    }
}
