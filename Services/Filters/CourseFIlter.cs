﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Services.Filters
{
    public class CourseFIlter:IFilter<Absence>
    {
        private int courseId;
        public CourseFIlter(int courseId)
        {
            this.courseId = courseId;
        }
        public List<Absence> filter(List<Absence> src)
        {
            return src.Where(x => x.courseId.Equals(this.courseId)).ToList();
        }
    }
}
