﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Services.Filters
{
    public class BeforeFIlter:IFilter<Absence>
    {
        private DateTime stop;
        public BeforeFIlter(DateTime stop)
        {
            this.stop = stop;
        }
        public List<Absence> filter(List<Absence> src)
        {
            var filterd = src.Where(x => x.date.CompareTo(this.stop) <= 0).ToList();
            return filterd;
        }
    }
}
