﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Filters
{
   public interface  IFilter <T>
   {
       List<T> filter(List<T> src);
   }
}
