﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Services.Filters
{
    public class AfterFilter:IFilter<Absence>
    {
        private DateTime start;
        public AfterFilter(DateTime start)
        {
            this.start = start;
        }
        public List<Absence> filter(List<Absence> src)
        {
            var filterd = src.Where(x => x.date.CompareTo(this.start) >= 0).ToList();
            return filterd;
        }

    }
}
