﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Dynamic;
using System.Linq;
using EntityState = System.Data.Entity.EntityState;

namespace DataAccess.IDao
{
    public abstract class AbstractDao<T, C>:IRepository<C> where C : class where T : DbContext, new()
    {

        private static T _entities=null;

        private T getDbContext()
        {
            if (_entities == null)
            {
               _entities = new T();
            }
            return _entities;
           
        }


        public void Add(C entity)
        {
            var conn = this.getDbContext();
            conn.Set<C>().Add(entity);
            conn.Entry(entity).State = EntityState.Added;
            var t = conn.Set<C>().ToList();
            this.Save();
        }
       public List<C> getAll()
        {
            var conn = this.getDbContext();
            return conn.Set<C>().ToList();
        }
        public void UpdateById(C entity,int id)

        {
            var conn = this.getDbContext();

            var toBeUpdated = _entities.Set<C>().Find(id);
            conn.Entry(toBeUpdated).CurrentValues.SetValues(entity);
            conn.SaveChanges();
        }

        public void Save()
        {
            var conn = this.getDbContext();
            try
            {
                conn.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var t = e.ToString();
            }
            catch (Exception e)
            {
                var t = e.ToString();
            }
        }

        public void Insert(C entity)
        {
            var conn = this.getDbContext();

            conn.Set<C>().Add(entity);
            this.Save();
        }

        public void UpdateById(C entity, object id)
        {
            var conn = this.getDbContext();

            var t = conn.Set<C>().Find(id);
            conn.Entry(t).CurrentValues.SetValues(entity);
            this.Save();
        }
        public  void DeleteById(object id)
       {
           var conn = this.getDbContext();

            try
            {

               var result = conn.Set<C>().Find(id);
               conn.Set<C>().Attach(result);
              
               conn.Set<C>().Remove(result);
               conn.SaveChanges();
                 //  _entities.Set<C>().Attach()
                 //  _entities.Set<C>().Remove(result);

                 this.Save();
           }
           catch (EntityException e)
           {
               var t = e.ToString();
           }
           catch (Exception e)
           {
               var t = e.ToString();
           }
        }
    }
}
